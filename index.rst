.. |label| replace:: Versende Status E-Mails
.. |snippet| replace:: FvStatusMails
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Dieses Plugin versendet die verknüpfte Status-Email (siehe Mailvorlagen) bei Änderung des Status.

Frontend
--------
keine

Backend
-------
Konfiguration
_____________
.. image:: FvStatusMails1.png
Hier können für jeden Status der im Shop existiert die automatische eMail aktivieren.

Cron-Job
________
Es werden Cron-Jobs angelegt mit der Bezeichnung
:Versende Status E-Mails: Dieser Cronjob versendet die eMails an die Kunden.

Emails-Vorlagen
_______________
.. image:: FvStatusMails2.png
Die eMail-Vorlagen können Sie ganz bequem im Backend selbst bearbeiten. Hier wird der Standard benutzt.

technische Beschreibung
------------------------
nachdem die email versendet wird, wir in der Datenbank in order_attribute die mail mit dem Status entsprechend gesetzt, so dass diese nicht mehrmals versendet wird.

Modifizierte Template-Dateien
-----------------------------
keine



